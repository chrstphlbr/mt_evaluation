# Evaluation project for hADL DSL

This project evaluates the [hADL DSL](https://bitbucket.org/chrstphlbr/mt_dsl) with a simplified [Scrum model](https://bitbucket.org/chrstphlbr/mt_evaluation_hadl). The [DSL script](https://bitbucket.org/chrstphlbr/mt_evaluation_dsl) specifies two scenarios, which are invoked from the Evaluation (generated hADL-Client code is included in the project). Furthermore the evaluation makes use of the [Agilefant](https://bitbucket.org/chrstphlbr/mt_agilefant4hadl), [Bitbucket and HipChat projects](https://bitbucket.org/chrstphlbr/mt_atlassian4hadl).

Prerequisites in order to run the scenario are mentioned in Chapter Evaluation (Section 5.1.5) of Christoph Laaber's [master thesis](https://bitbucket.org/chrstphlbr/mt_thesis).

## Required Projects
The following dependancies are not available through a public Maven repository. Hence one must install them to the local Maven repository before executing the Evaluation.
 
 * [hADL Schema and Runtime](https://bitbucket.org/christophdorn/hadl)
 * [hADL Synchronous Library](https://bitbucket.org/chrstphlbr/mt_dsl-lib)
 * [Agilefant Surrogates and Sensors](https://bitbucket.org/chrstphlbr/mt_agilefant4hadl)
 * [Bitbucket and HipChat Surrogates and Sensors](https://bitbucket.org/chrstphlbr/mt_atlassian4hadl)

## Starting Evaluation
First install the project by running the following command from terminal:
```
mvn clean install
```
To start the scenario simply run the tests from the base directory of the project:
```
mvn test
```

## Not necessarily required Projects
 * [DSL](https://bitbucket.org/chrstphlbr/mt_dsl) for hADL: because DSL script is copied into the project
 * [Scrum DSL Script](https://bitbucket.org/chrstphlbr/mt_evaluation_dsl): because DSL script is copied into the project
 * [Scrum hADL Model](https://bitbucket.org/chrstphlbr/mt_evaluation_hadl): because hADL model is copied into the project