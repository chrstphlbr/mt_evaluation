package net.laaber.mt.evaluation;

import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabLink;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import java.util.List;
import java.util.Map;
import net.laaber.mt.dsl.lib.hadl.ProcessScope;

@SuppressWarnings("all")
public class Tasks {
  private ProcessScope setUp() throws Throwable {
    final net.laaber.mt.dsl.lib.hadl.HadlModelInteractor mi = new net.laaber.mt.dsl.lib.hadl.HadlModelInteractorImpl();
    fj.data.Either<Throwable, at.ac.tuwien.dsg.hadl.schema.core.HADLmodel> loadResult = mi.load("/Users/Christoph/TU/Diplomarbeit/Code/Evaluation/src/main/resources/agile-hadl.xml");
    if (loadResult.isLeft()) {
    	throw loadResult.left().value();
    }
    final at.ac.tuwien.dsg.hadl.schema.core.HADLmodel model = loadResult.right().value();
    
    final com.google.inject.Injector baseInjector = com.google.inject.Guice.createInjector(new com.google.inject.AbstractModule() {
    	@Override
    	protected void configure() {
    	}
    	
    	@com.google.inject.Provides
    	@javax.inject.Singleton
    	at.ac.tuwien.dsg.hadl.framework.runtime.impl.Executable2OperationalTransformer getTransformer() {
    		return new at.ac.tuwien.dsg.hadl.framework.runtime.impl.Executable2OperationalTransformer();
    	}
    	
    	@com.google.inject.Provides
    	@javax.inject.Singleton
    	at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil getTypesUtil() {
    		at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil mtu = new at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil();
    		mtu.init(model);
    		return mtu;
    	}
    });
    
    final com.google.inject.Injector injector = com.google.inject.Guice.createInjector(new com.google.inject.AbstractModule() {
    	@Override
    	protected void configure() {
    		bind(at.ac.tuwien.dsg.hadl.framework.runtime.impl.HADLLinkageConnector.class).in(javax.inject.Singleton.class);
    		bind(at.ac.tuwien.dsg.hadl.framework.runtime.impl.HADLruntimeMonitor.class).in(javax.inject.Singleton.class);
    		bind(net.laaber.mt.dsl.lib.hadl.HadlLinker.class).to(net.laaber.mt.dsl.lib.hadl.HadlLinkerImpl.class).in(javax.inject.Singleton.class);
    		bind(net.laaber.mt.dsl.lib.hadl.HadlMonitor.class).to(net.laaber.mt.dsl.lib.hadl.HadlMonitorImpl.class).in(javax.inject.Singleton.class);
    		bind(at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorFactory.class).to(net.laaber.mt.evaluation.sensor.DslSensorFactory.class).in(javax.inject.Singleton.class);
    		bind(ProcessScope.class).in(javax.inject.Singleton.class);
    	}
    	
    	@com.google.inject.Provides
    	@javax.inject.Singleton
    	net.laaber.mt.dsl.lib.hadl.HadlModelInteractor getModelInteractor() {
    		return mi;
    	}
    	
    	@com.google.inject.Provides
    	@javax.inject.Singleton
    	at.ac.tuwien.dsg.hadl.framework.runtime.impl.Executable2OperationalTransformer getTransformer() {
    		return baseInjector.getInstance(at.ac.tuwien.dsg.hadl.framework.runtime.impl.Executable2OperationalTransformer.class);
    	}
    	
    	@com.google.inject.Provides
    	@javax.inject.Singleton
    	at.ac.tuwien.dsg.hadl.framework.runtime.impl.RuntimeRegistry getRegistry() {
    		return new at.ac.tuwien.dsg.hadl.framework.runtime.impl.RuntimeRegistry();
    	}
    	
    	@com.google.inject.Provides
    	@javax.inject.Singleton
    	at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil getTypesUtil() {
    		return baseInjector.getInstance(at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil.class);
    	}
    	
    	@com.google.inject.Provides
    	@javax.inject.Singleton
    	at.ac.tuwien.dsg.hadl.framework.runtime.impl.SurrogateFactoryResolver getFactoryResolver() {
    		at.ac.tuwien.dsg.hadl.framework.runtime.impl.SurrogateFactoryResolver sfr = new at.ac.tuwien.dsg.hadl.framework.runtime.impl.SurrogateFactoryResolver();
    		try {
    			sfr.resolveFactoriesFrom(model);
    		} catch (at.ac.tuwien.dsg.hadl.framework.runtime.intf.FactoryResolvingException e) {
    			e.printStackTrace();
    		}
    		return sfr;
    	}
    	
    	@com.google.inject.Provides
    	@javax.inject.Singleton
    	at.ac.tuwien.dsg.hadl.framework.runtime.impl.HADLruntimeModel getRuntimeModel() {
    		at.ac.tuwien.dsg.hadl.framework.runtime.impl.Neo4JbackedRuntimeModel hrm = new at.ac.tuwien.dsg.hadl.framework.runtime.impl.Neo4JbackedRuntimeModel();
    		baseInjector.injectMembers(hrm);
    		// setup Neo4J DB
    		String dbPath = "neo4j-store";
    		org.apache.commons.io.FileUtils.deleteQuietly(new java.io.File(dbPath));
    		org.neo4j.graphdb.GraphDatabaseService graphDb = new org.neo4j.graphdb.factory.GraphDatabaseFactory().newEmbeddedDatabase(dbPath);
    		hrm.init(graphDb, model);
    		return hrm;  							
    	}
    }); 
    
    net.laaber.mt.dsl.lib.hadl.HadlLinker linker = injector.getInstance(net.laaber.mt.dsl.lib.hadl.HadlLinker.class);
    
    fj.data.Option<Throwable> setUpError = linker.setUp(null);
    if (setUpError.isSome()) {
    	throw new RuntimeException(setUpError.some().getCause());
    }
    
    return injector.getInstance(ProcessScope.class);
  }
  
  public Map<String, Object> sprint(final ProcessScope processScope, final Integer id, final String name) throws Throwable {
    ProcessScope ps = processScope;
    if (ps == null) {
    	ps = setUp();
    }
    // Begin hADL Input checks
    // End hADL Input checks
    final Map<String, Object> ret = new java.util.HashMap<>();
    ret.put("processScope", ps);
    final List<net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure> failureList = new java.util.ArrayList<>();
    ret.put("failureList", failureList);
    
    // Begin Create Statement
    final at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor rd1 = net.laaber.mt.hadl.agilefant.resourceDescriptor.AgilefantResourceDescriptorFactory.agilefant(name, id);
    final fj.data.Either<Throwable, at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement> acquireResult1 = ps.getLinker().acquire("scrum.obj.Sprint", rd1);
    if (acquireResult1.isLeft()) {
    	if (acquireResult1.left().value() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) acquireResult1.left().value());
    		return ret;
    	} else {
    		throw acquireResult1.left().value();
    	}
    }
    final TOperationalObject s = (TOperationalObject) acquireResult1.right().value();
    ret.put("sprint", s);
    // End Create Statement
    return ret;
  }
  
  public Map<String, Object> createChatsForStoriesOfSprint(final ProcessScope processScope, final TOperationalObject s) throws Throwable {
    ProcessScope ps = processScope;
    if (ps == null) {
    	ps = setUp();
    }
    // Begin hADL Input checks
    if (!ps.getModelTypesUtil().isTypeSupertypeOf(ps.getModelTypesUtil().getById("scrum.obj.Sprint"), ps.getModelTypesUtil().getByTypeRef(s.getInstanceOf()))) {
    	throw new IllegalArgumentException("s is not a scrum.obj.Sprint");
    }
    // End hADL Input checks
    final Map<String, Object> ret = new java.util.HashMap<>();
    ret.put("processScope", ps);
    final List<net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure> failureList = new java.util.ArrayList<>();
    ret.put("failureList", failureList);
    
    // Begin StopScope Statement
    final fj.data.Option<Throwable> stopScopeResult2 = ps.getLinker().stopScope();
    if (stopScopeResult2.isSome()) {
    	if (stopScopeResult2.some() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) stopScopeResult2.some());
    		return ret;
    	} else {
    		throw stopScopeResult2.some();
    	}
    }
    // End StopScope Statement
    
    // Begin Load Statement
    final List<Map.Entry<String, String>> vias3 = java.util.Arrays.asList();
    final fj.data.Either<Throwable, List<at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement>> loadResult3 = ps.getMonitor().load(new java.util.AbstractMap.SimpleEntry("scrum.obj.Story", "scrum.obj.containsStory"), s, vias3);
    if (loadResult3.isLeft()) {
    	if (loadResult3.left().value() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) loadResult3.left().value());
    		return ret;
    	} else {
    		throw loadResult3.left().value();
    	}
    }
    // convert List<THADLarchElement> to List<TOperationalObject> 
    final List<TOperationalObject> stories = new java.util.ArrayList<>();
    for (at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement i3 : loadResult3.right().value()) {
    	stories.add((TOperationalObject) i3);
    }
    // End Load Statement
    
    // Begin HadlVariableDeclaration Statement
    // List of hADL type scrum.obj.Chat
    List<TOperationalObject> chats = new java.util.ArrayList<>();
    ret.put("chats", chats);
    // End HadlVariableDeclaration Statement
    
    // Begin HadlVariableDeclaration Statement
    // List of hADL type scrum.links.invite
    List<TOperationalCollabLink> invites = new java.util.ArrayList<>();
    ret.put("invites", invites);
    // End HadlVariableDeclaration Statement
    
    // Begin Iteration Statement
    for (int i = 0; i < stories.size(); i++) {
    	final TOperationalObject story = stories.get(i);
    	
    	// Begin Create Statement
    	final at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor rd5 = net.laaber.mt.hadl.atlassian.resourceDescriptor.AtlassianResourceDescriptorFactory.hipChat("StoryChat" + i, "StoryChat" + i);
    	final fj.data.Either<Throwable, at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement> acquireResult5 = ps.getLinker().acquire("scrum.obj.Chat", rd5);
    	if (acquireResult5.isLeft()) {
    		if (acquireResult5.left().value() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    			failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) acquireResult5.left().value());
    			continue;
    		} else {
    			throw acquireResult5.left().value();
    		}
    	}
    	final TOperationalObject c = (TOperationalObject) acquireResult5.right().value();
    	// End Create Statement
    	
    	// Begin HadlListVariableAppend Statement
    	chats.add(c);
    	// End HadlListVariableAppend Statement
    	
    	// Begin Load Statement
    	final List<Map.Entry<String, String>> vias6 = java.util.Arrays.asList(new java.util.AbstractMap.SimpleEntry("scrum.col.AgileUser", "scrum.links.responsible"));
    	final fj.data.Either<Throwable, List<at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement>> loadResult6 = ps.getMonitor().load(new java.util.AbstractMap.SimpleEntry("scrum.col.DevUser", "scrum.col.devUser"), story, vias6);
    	if (loadResult6.isLeft()) {
    		if (loadResult6.left().value() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    			failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) loadResult6.left().value());
    			continue;
    		} else {
    			throw loadResult6.left().value();
    		}
    	}
    	// convert List<THADLarchElement> to List<TOperationalComponent> 
    	final List<at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent> users = new java.util.ArrayList<>();
    	for (at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement i6 : loadResult6.right().value()) {
    		users.add((at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent) i6);	
    	}
    	// End Load Statement
    	
    	// Begin Iteration Statement
    	for (int i7 = 0; i7 < users.size(); i7++) {
    		final at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent u = users.get(i7);
    		
    		// Begin Link Statement
    		final fj.data.Either<Throwable, TOperationalCollabLink> linkResult8 = ps.getLinker().link((at.ac.tuwien.dsg.hadl.schema.core.TCollaborator) u, (TOperationalObject) c, "scrum.links.invite");
    		if (linkResult8.isLeft()) {
    			if (linkResult8.left().value() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    				failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) linkResult8.left().value());
    				continue;
    			} else {
    				throw linkResult8.left().value();
    			}
    		}
    		final TOperationalCollabLink invite = linkResult8.right().value();
    		// End Link Statement
    		
    		// Begin HadlListVariableAppend Statement
    		invites.add(invite);
    		// End HadlListVariableAppend Statement
    	}
    	if (!failureList.isEmpty()) {
    		return ret;
    	}
    	// End Iteration Statement
    }
    if (!failureList.isEmpty()) {
    	return ret;
    }
    // End Iteration Statement
    
    // Begin StopScope Statement
    final fj.data.Option<Throwable> startScopeResult9 = ps.getLinker().startScope();
    if (startScopeResult9.isSome()) {
    	if (startScopeResult9.some() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) startScopeResult9.some());
    		return ret;
    	} else {
    		throw startScopeResult9.some();
    	}
    }
    // End StopScope Statement
    return ret;
  }
  
  public Map<String, Object> releaseChatsAndInvites(final ProcessScope processScope, final List<TOperationalObject> chats, final List<TOperationalCollabLink> invites) throws Throwable {
    ProcessScope ps = processScope;
    if (ps == null) {
    	ps = setUp();
    }
    // Begin hADL Input checks
    final at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement st1 = ps.getModelTypesUtil().getById("scrum.obj.Chat");
    for (int i1 = 0; i1 < chats.size(); i1++) {
    	if (!ps.getModelTypesUtil().isTypeSupertypeOf(st1, ps.getModelTypesUtil().getByTypeRef(chats.get(i1).getInstanceOf()))) {
    		throw new IllegalArgumentException("chats does not contain just elements of scrum.obj.Chat");
    	}
    }
    final at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement st2 = ps.getModelTypesUtil().getById("scrum.links.invite");
    for (int i2 = 0; i2 < invites.size(); i2++) {
    	if (!ps.getModelTypesUtil().isTypeSupertypeOf(st2, ps.getModelTypesUtil().getByTypeRef(invites.get(i2).getInstanceOf()))) {
    		throw new IllegalArgumentException("invites does not contain just elements of scrum.links.invite");
    	}
    }
    // End hADL Input checks
    final Map<String, Object> ret = new java.util.HashMap<>();
    ret.put("processScope", ps);
    final List<net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure> failureList = new java.util.ArrayList<>();
    ret.put("failureList", failureList);
    
    // Begin StopScope Statement
    final fj.data.Option<Throwable> stopScopeResult3 = ps.getLinker().stopScope();
    if (stopScopeResult3.isSome()) {
    	if (stopScopeResult3.some() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) stopScopeResult3.some());
    		return ret;
    	} else {
    		throw stopScopeResult3.some();
    	}
    }
    // End StopScope Statement
    
    // Begin Iteration Statement
    for (int i4 = 0; i4 < invites.size(); i4++) {
    	final TOperationalCollabLink i = invites.get(i4);
    	
    	// Begin Unlink Statement
    	final fj.data.Option<Throwable> unlinkResult5 = ps.getLinker().unlink(i);
    	if (unlinkResult5.isSome()) {
    		if (unlinkResult5.some() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    			failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) unlinkResult5.some());
    			continue;
    		} else {
    			throw unlinkResult5.some();
    		}
    	}
    	// End Unlink Statement
    }
    if (!failureList.isEmpty()) {
    	return ret;
    }
    // End Iteration Statement
    
    // Begin StopScope Statement
    final fj.data.Option<Throwable> startScopeResult6 = ps.getLinker().startScope();
    if (startScopeResult6.isSome()) {
    	if (startScopeResult6.some() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) startScopeResult6.some());
    		return ret;
    	} else {
    		throw startScopeResult6.some();
    	}
    }
    // End StopScope Statement
    
    // Begin Iteration Statement
    for (int i7 = 0; i7 < chats.size(); i7++) {
    	final TOperationalObject c = chats.get(i7);
    	
    	// Begin Release Statement
    	final fj.data.Option<Throwable> releaseResult8 = ps.getLinker().release(c);
    	if (releaseResult8.isSome()) {
    		if (releaseResult8.some() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    			failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) releaseResult8.some());
    			continue;
    		} else {
    			throw releaseResult8.some();
    		}
    	}
    	// End Release Statment
    }
    if (!failureList.isEmpty()) {
    	return ret;
    }
    // End Iteration Statement
    return ret;
  }
  
  public Map<String, Object> sprintRetrospectiveSetUp(final ProcessScope processScope, final TOperationalObject sprint, final Integer sprintNumber, final Integer previousSprintNumber) throws Throwable {
    ProcessScope ps = processScope;
    if (ps == null) {
    	ps = setUp();
    }
    // Begin hADL Input checks
    if (!ps.getModelTypesUtil().isTypeSupertypeOf(ps.getModelTypesUtil().getById("scrum.obj.Sprint"), ps.getModelTypesUtil().getByTypeRef(sprint.getInstanceOf()))) {
    	throw new IllegalArgumentException("sprint is not a scrum.obj.Sprint");
    }
    // End hADL Input checks
    final Map<String, Object> ret = new java.util.HashMap<>();
    ret.put("processScope", ps);
    final List<net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure> failureList = new java.util.ArrayList<>();
    ret.put("failureList", failureList);
    
    // Begin StopScope Statement
    final fj.data.Option<Throwable> stopScopeResult2 = ps.getLinker().stopScope();
    if (stopScopeResult2.isSome()) {
    	if (stopScopeResult2.some() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) stopScopeResult2.some());
    		return ret;
    	} else {
    		throw stopScopeResult2.some();
    	}
    }
    // End StopScope Statement
    
    // Begin Create Statement
    final at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor rd3 = net.laaber.mt.hadl.atlassian.resourceDescriptor.AtlassianResourceDescriptorFactory.bitbucket("SprintWiki" + sprintNumber, "SprintWiki" + sprintNumber);
    final fj.data.Either<Throwable, at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement> acquireResult3 = ps.getLinker().acquire("scrum.obj.Wiki", rd3);
    if (acquireResult3.isLeft()) {
    	if (acquireResult3.left().value() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) acquireResult3.left().value());
    		return ret;
    	} else {
    		throw acquireResult3.left().value();
    	}
    }
    final TOperationalObject currentWiki = (TOperationalObject) acquireResult3.right().value();
    ret.put("wiki", currentWiki);
    // End Create Statement
    
    // Begin Create Statement
    final at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor rd4 = net.laaber.mt.hadl.atlassian.resourceDescriptor.AtlassianResourceDescriptorFactory.bitbucket("SprintWiki" + previousSprintNumber, "SprintWiki" + previousSprintNumber);
    final fj.data.Either<Throwable, at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement> acquireResult4 = ps.getLinker().acquire("scrum.obj.Wiki", rd4);
    if (acquireResult4.isLeft()) {
    	if (acquireResult4.left().value() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) acquireResult4.left().value());
    		return ret;
    	} else {
    		throw acquireResult4.left().value();
    	}
    }
    final TOperationalObject previousWiki = (TOperationalObject) acquireResult4.right().value();
    // End Create Statement
    
    // Begin Reference Statement
    final fj.data.Either<Throwable, at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObjectRef> referenceResult5 = ps.getLinker().reference(currentWiki, previousWiki, "scrum.obj.prev");
    if (referenceResult5.isLeft()) {
    	if (referenceResult5.left().value() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) referenceResult5.left().value());
    		return ret;
    	} else {
    		throw referenceResult5.left().value();
    	}
    }
    final at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObjectRef p1 = referenceResult5.right().value();
    // End Reference Statement
    
    // Begin Reference Statement
    final fj.data.Either<Throwable, at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObjectRef> referenceResult6 = ps.getLinker().reference(previousWiki, currentWiki, "scrum.obj.next");
    if (referenceResult6.isLeft()) {
    	if (referenceResult6.left().value() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) referenceResult6.left().value());
    		return ret;
    	} else {
    		throw referenceResult6.left().value();
    	}
    }
    final at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObjectRef p2 = referenceResult6.right().value();
    // End Reference Statement
    
    // Begin StopScope Statement
    final fj.data.Option<Throwable> startScopeResult7 = ps.getLinker().startScope();
    if (startScopeResult7.isSome()) {
    	if (startScopeResult7.some() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) startScopeResult7.some());
    		return ret;
    	} else {
    		throw startScopeResult7.some();
    	}
    }
    // End StopScope Statement
    
    // Begin Create Statement
    final at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor rd8 = net.laaber.mt.hadl.atlassian.resourceDescriptor.AtlassianResourceDescriptorFactory.hipChat("RetrospectiveChat" + sprintNumber, "RetrospectiveChat" + sprintNumber);
    final fj.data.Either<Throwable, at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement> acquireResult8 = ps.getLinker().acquire("scrum.obj.Chat", rd8);
    if (acquireResult8.isLeft()) {
    	if (acquireResult8.left().value() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) acquireResult8.left().value());
    		return ret;
    	} else {
    		throw acquireResult8.left().value();
    	}
    }
    final TOperationalObject chat = (TOperationalObject) acquireResult8.right().value();
    ret.put("chat", chat);
    // End Create Statement
    
    // Begin Load Statement
    final List<Map.Entry<String, String>> vias9 = java.util.Arrays.asList(new java.util.AbstractMap.SimpleEntry("scrum.obj.Story", "scrum.obj.containsStory"), new java.util.AbstractMap.SimpleEntry("scrum.col.AgileUser", "scrum.links.responsible"));
    final fj.data.Either<Throwable, List<at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement>> loadResult9 = ps.getMonitor().load(new java.util.AbstractMap.SimpleEntry("scrum.col.DevUser", "scrum.col.devUser"), sprint, vias9);
    if (loadResult9.isLeft()) {
    	if (loadResult9.left().value() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) loadResult9.left().value());
    		return ret;
    	} else {
    		throw loadResult9.left().value();
    	}
    }
    // convert List<THADLarchElement> to List<TOperationalComponent> 
    final List<at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent> users = new java.util.ArrayList<>();
    for (at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement i9 : loadResult9.right().value()) {
    	users.add((at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent) i9);	
    }
    // End Load Statement
    
    // Begin StopScope Statement
    final fj.data.Option<Throwable> stopScopeResult10 = ps.getLinker().stopScope();
    if (stopScopeResult10.isSome()) {
    	if (stopScopeResult10.some() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) stopScopeResult10.some());
    		return ret;
    	} else {
    		throw stopScopeResult10.some();
    	}
    }
    // End StopScope Statement
    
    // Begin HadlVariableDeclaration Statement
    // List of hADL type scrum.links.invite
    List<TOperationalCollabLink> invites = new java.util.ArrayList<>();
    ret.put("invites", invites);
    // End HadlVariableDeclaration Statement
    
    // Begin Iteration Statement
    for (int i11 = 0; i11 < users.size(); i11++) {
    	final at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent u = users.get(i11);
    	
    	// Begin Link Statement
    	final fj.data.Either<Throwable, TOperationalCollabLink> linkResult12 = ps.getLinker().link((at.ac.tuwien.dsg.hadl.schema.core.TCollaborator) u, (TOperationalObject) chat, "scrum.links.invite");
    	if (linkResult12.isLeft()) {
    		if (linkResult12.left().value() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    			failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) linkResult12.left().value());
    			continue;
    		} else {
    			throw linkResult12.left().value();
    		}
    	}
    	final TOperationalCollabLink i = linkResult12.right().value();
    	// End Link Statement
    	
    	// Begin HadlListVariableAppend Statement
    	invites.add(i);
    	// End HadlListVariableAppend Statement
    }
    if (!failureList.isEmpty()) {
    	return ret;
    }
    // End Iteration Statement
    
    // Begin StopScope Statement
    final fj.data.Option<Throwable> startScopeResult13 = ps.getLinker().startScope();
    if (startScopeResult13.isSome()) {
    	if (startScopeResult13.some() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) startScopeResult13.some());
    		return ret;
    	} else {
    		throw startScopeResult13.some();
    	}
    }
    // End StopScope Statement
    
    // Begin Release Statement
    final fj.data.Option<Throwable> releaseResult14 = ps.getLinker().release(previousWiki);
    if (releaseResult14.isSome()) {
    	if (releaseResult14.some() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) releaseResult14.some());
    		return ret;
    	} else {
    		throw releaseResult14.some();
    	}
    }
    // End Release Statment
    return ret;
  }
  
  public Map<String, Object> sprintRetrospectiveTearDown(final ProcessScope processScope, final TOperationalObject chat, final List<TOperationalCollabLink> invites) throws Throwable {
    ProcessScope ps = processScope;
    if (ps == null) {
    	ps = setUp();
    }
    // Begin hADL Input checks
    if (!ps.getModelTypesUtil().isTypeSupertypeOf(ps.getModelTypesUtil().getById("scrum.obj.Chat"), ps.getModelTypesUtil().getByTypeRef(chat.getInstanceOf()))) {
    	throw new IllegalArgumentException("chat is not a scrum.obj.Chat");
    }
    final at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement st2 = ps.getModelTypesUtil().getById("scrum.links.invite");
    for (int i2 = 0; i2 < invites.size(); i2++) {
    	if (!ps.getModelTypesUtil().isTypeSupertypeOf(st2, ps.getModelTypesUtil().getByTypeRef(invites.get(i2).getInstanceOf()))) {
    		throw new IllegalArgumentException("invites does not contain just elements of scrum.links.invite");
    	}
    }
    // End hADL Input checks
    final Map<String, Object> ret = new java.util.HashMap<>();
    ret.put("processScope", ps);
    final List<net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure> failureList = new java.util.ArrayList<>();
    ret.put("failureList", failureList);
    
    // Begin StopScope Statement
    final fj.data.Option<Throwable> stopScopeResult3 = ps.getLinker().stopScope();
    if (stopScopeResult3.isSome()) {
    	if (stopScopeResult3.some() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) stopScopeResult3.some());
    		return ret;
    	} else {
    		throw stopScopeResult3.some();
    	}
    }
    // End StopScope Statement
    
    // Begin Iteration Statement
    for (int i4 = 0; i4 < invites.size(); i4++) {
    	final TOperationalCollabLink i = invites.get(i4);
    	
    	// Begin Unlink Statement
    	final fj.data.Option<Throwable> unlinkResult5 = ps.getLinker().unlink(i);
    	if (unlinkResult5.isSome()) {
    		if (unlinkResult5.some() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    			failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) unlinkResult5.some());
    			continue;
    		} else {
    			throw unlinkResult5.some();
    		}
    	}
    	// End Unlink Statement
    }
    if (!failureList.isEmpty()) {
    	return ret;
    }
    // End Iteration Statement
    
    // Begin StopScope Statement
    final fj.data.Option<Throwable> startScopeResult6 = ps.getLinker().startScope();
    if (startScopeResult6.isSome()) {
    	if (startScopeResult6.some() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) startScopeResult6.some());
    		return ret;
    	} else {
    		throw startScopeResult6.some();
    	}
    }
    // End StopScope Statement
    
    // Begin Release Statement
    final fj.data.Option<Throwable> releaseResult7 = ps.getLinker().release(chat);
    if (releaseResult7.isSome()) {
    	if (releaseResult7.some() instanceof net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) {
    		failureList.add((net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure) releaseResult7.some());
    		return ret;
    	} else {
    		throw releaseResult7.some();
    	}
    }
    // End Release Statment
    return ret;
  }
}
