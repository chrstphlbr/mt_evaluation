package net.laaber.mt.evaluation.sensor;

import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollabObjectSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollabSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollaboratorSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.InsufficientModelInformationException;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabObject;
import at.ac.tuwien.dsg.hadl.schema.core.TCollaborator;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import net.laaber.mt.hadl.agilefant.sensor.AgilefantSensorFactory;
import net.laaber.mt.hadl.atlassian.sensor.AtlassianSensorFactory;

import javax.inject.Inject;
import java.io.IOException;

public class DslSensorFactory implements at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorFactory {

    private final AtlassianSensorFactory atlassian;
    private final AgilefantSensorFactory agilefant;

    @Inject
    public DslSensorFactory(ModelTypesUtil mtu) throws IOException {
        atlassian = new AtlassianSensorFactory(mtu);
        agilefant = new AgilefantSensorFactory(mtu);
    }


	@Override
	public ICollabSensor getInstance(THADLarchElement thadLarchElement) throws InsufficientModelInformationException {
        if (thadLarchElement instanceof TCollaborator) {
            return getInstance((TCollaborator) thadLarchElement);
        } else if (thadLarchElement instanceof TCollabObject) {
            return getInstance((TCollabObject) thadLarchElement);
        }
        return null;
	}

	@Override
	public ICollaboratorSensor getInstance(TCollaborator tCollaborator) throws InsufficientModelInformationException {
        ICollaboratorSensor s = atlassian.getInstance(tCollaborator);
        if (s == null) {
            s = agilefant.getInstance(tCollaborator);
        }
        return s;
	}

	@Override
	public ICollabObjectSensor getInstance(TCollabObject tCollabObject) throws InsufficientModelInformationException {
        ICollabObjectSensor s = atlassian.getInstance(tCollabObject);
        if (s == null) {
            s = agilefant.getInstance(tCollabObject);
        }
        return s;
	}
}