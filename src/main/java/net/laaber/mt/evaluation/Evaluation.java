package net.laaber.mt.evaluation;

import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.runtime.*;
import net.laaber.mt.dsl.lib.hadl.ProcessScope;
import net.laaber.mt.dsl.lib.hadl.SurrogateStateChangeFailure;

import java.time.Duration;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph on 16.11.15.
 */
public class Evaluation {
    // copied from agilefant web portal
    private static final Integer sprintId = 156935;

    private static final String processScopeKey = "processScope";
    private static final String failureListKey = "failureList";

    private static final long wait = Duration.ofMinutes(3).toMillis();

    // This method represents a hardcoded process that utilises the generated tasks by the DSL.
    public void process() throws Throwable {
        Tasks t = new Tasks();

        Map<String, Object> out = t.sprint(null, sprintId, "Sprint1");
        ProcessScope ps = (ProcessScope) out.get(processScopeKey);
        handleFailures(ps, (List<SurrogateStateChangeFailure>) out.get(failureListKey));
        TOperationalObject sprint = (TOperationalObject) out.get("sprint");

        System.out.println("loaded sprint " + sprintId);

        out = t.createChatsForStoriesOfSprint(ps, sprint);
        ps = (ProcessScope) out.get(processScopeKey);
        handleFailures(ps, (List<SurrogateStateChangeFailure>) out.get(failureListKey));
        List<TOperationalObject> chats = (List<TOperationalObject>) out.get("chats");
        List<TOperationalCollabLink> invites = (List<TOperationalCollabLink>) out.get("invites");

        System.out.println("created chats for stories");
        System.out.println("number of chats=" + chats.size() + "; number of invites=" + invites.size());

        Thread.sleep(wait);

        out = t.releaseChatsAndInvites(ps, chats, invites);
        ps = (ProcessScope) out.get(processScopeKey);
        handleFailures(ps, (List<SurrogateStateChangeFailure>) out.get(failureListKey));

        System.out.println("released chats for stories");

        Thread.sleep(wait);

        out = t.sprintRetrospectiveSetUp(ps, sprint, 1, 0);
        ps = (ProcessScope) out.get(processScopeKey);
        handleFailures(ps, (List<SurrogateStateChangeFailure>) out.get(failureListKey));
        TOperationalObject retrospectiveChat = (TOperationalObject) out.get("chat");
        TOperationalObject retrospectiveWiki = (TOperationalObject) out.get("wiki");
        List<TOperationalCollabLink> retrospectiveInvites = (List<TOperationalCollabLink>) out.get("invites");

        System.out.println("created retrospective");
        System.out.println("number of invites=" + retrospectiveInvites.size());

        Thread.sleep(wait);

        out = t.sprintRetrospectiveTearDown(ps, retrospectiveChat, retrospectiveInvites);
        ps = (ProcessScope) out.get(processScopeKey);
        handleFailures(ps, (List<SurrogateStateChangeFailure>) out.get(failureListKey));

        System.out.println("released retrospective");
    }

    private void handleFailures(ProcessScope ps, List<SurrogateStateChangeFailure> failures) throws Exception {
        for (SurrogateStateChangeFailure f : failures) {
            System.out.println("Failure occurred: " + f.getMessage());
            try {
                THADLarchElement op = f.surrogateEvent().getOperationalElement();
                System.out.println("  hADL-Type: " + ps.getModelTypesUtil().getByTypeRef(type(op)).getId());
                System.out.println("  RD-id: " + rd(op).getId());
                System.out.println("  ErrorType: " + f.surrogateEvent().getOptionalEx().getErrType());
                f.surrogateEvent().getOptionalEx().printStackTrace();
            } catch (Throwable t) {
            }
        }
        if (!failures.isEmpty()) {
            throw new Exception(failures.size() + " failures occurred");
        }
    }

    private TResourceDescriptor rd(THADLarchElement operational) {
        TResourceDescriptor rd = null;
        if (operational instanceof TOperationalObject) {
            rd = ((TOperationalObject) operational).getResourceDescriptor().get(0);
        } else if (operational instanceof TOperationalComponent) {
            rd = ((TOperationalComponent) operational).getResourceDescriptor().get(0);
        } else if (operational instanceof TOperationalConnector) {
            rd = ((TOperationalConnector) operational).getResourceDescriptor().get(0);
        } else {
            return null;
        }
        return rd;
    }

    private TTypeRef type(THADLarchElement operational) {
        TTypeRef typeRef = null;
        if (operational instanceof TOperationalObject) {
            typeRef = ((TOperationalObject) operational).getInstanceOf();
        } else if (operational instanceof TOperationalComponent) {
            typeRef = ((TOperationalComponent) operational).getInstanceOf();
        } else if (operational instanceof TOperationalConnector) {
            typeRef = ((TOperationalConnector) operational).getInstanceOf();
        } else {
            return null;
        }
        return typeRef;
    }
}
