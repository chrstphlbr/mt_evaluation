package net.laaber.mt.evaluation;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Christoph on 16.11.15.
 */
public class EvaluationTest {
    @Test
    public void runEvaluation() {
        try {
            Evaluation e = new Evaluation();
            e.process();
        } catch (Throwable t) {
            t.printStackTrace();
            Assert.fail(t.getMessage());
        }
    }
}
